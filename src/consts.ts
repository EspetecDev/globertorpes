/**
 * Site metadata that is used across the site.
 *
 * A few of these are not used yet, and are subject to change, example of this is Author.
 */
export const SITE_METADATA = {
    title: 'Globertorpes Team',
    headerTitle: 'Globertorpes Team',
    description: 'AYR League - 2023/2024',
    language: 'en-us',
    theme: 'system', // Options: system, light, dark, Does not work yet
    siteUrl: 'https://globertorpes-espetecdev-f10c895dd7f61ab55ef9063ad23b6f704a3c071.gitlab.io/',
    // siteUrl: 'https://espetecdev.gitlab.io/',
    siteRepo: 'https://github.com/wanoo21/tailwind-astro-starting-blog',
    siteLogo: '/static/images/logo2.png',
    socialBanner: '/static/images/twitter-card.png',
    locale: 'en-US',

    // The following are subject to change. They are placeholders for now.
    // This project provide a default author content see src/content/authors/default.mdx, so these details are better to be inserted there.
    author: 'John Doe',
    mastodon: 'https://mastodon.social/@mastodonuser',
    email: 'address@yoursite.com',
    github: 'https://github.com',
    twitter: 'https://twitter.com/Twitter',
    facebook: 'https://facebook.com',
    youtube: 'https://youtube.com',
    linkedin: 'https://www.linkedin.com',

    eye_open: '<svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 48 48"><defs><style>.cls-1{fill:none;stroke:#000;stroke-linecap:round;stroke-linejoin:round;stroke-width:2px}</style></defs><circle class="cls-1" cx="24" cy="24" r="5"/><path class="cls-1" d="M44 24s-7 10-20 10S4 24 4 24s7-10 20-10 20 10 20 10z"/><path class="cls-1" d="M-360-496h700v700h-700z"/></svg>',
    eye_closed: '<svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 48 48"><defs><style>.cls-1{fill:none;stroke:#000;stroke-linecap:round;stroke-linejoin:round;stroke-width:2px}</style></defs><path class="cls-1" d="M4 16c3 0 5 14 20 14s17-14 20-14M24 32v-1M27.87 31.98l.5-1.88M35 29l-1-1M32 31v-2M38 27l-1-1M40 24l-1-1M20.13 31.98l-.5-1.88M13 29l1-1M16 31v-2M10 27l1-1M8 24l1-1M7 18h34"/><path class="cls-1" d="M-564-496h700v700h-700z"/></svg>',
};

/**
 * Default posts per page for pagination.
 */
export const ITEMS_PER_PAGE = 5;

export const NAVIGATION = [
    {href: '/', title: 'Home'},
    {href: '/partits', title: 'Partits'},
    {href: '/blog', title: 'Croniques'},
    {href: '/tecniques', title: 'Tècniques'},
    {href: '/playoff', title: 'PlayIn/PlayOff'},
    // {href: '/fotos', title: 'Fotos'},
    // {href: '/about', title: 'About'},
]

export const enum EQUIPS {
    Canaris = "CANARIS",
    Ayr = "AYR",
    Joan23 = "ESCOLA JOAN XXIII",
    Riferez = "RIFEREZ",
    LionsSP = "LIONS SPSP",
    Basf = "BASF OLD STARS",
    Toro = "TRANSPORTES O. TORO",
    Outsiders = "OUTSIDERS",
    LastGame = "THE LAST GAME",
    Covestro = "A.B. COVESTRO",
    GhostBill = "GHOST BILL'S",
    Globertorpes = "GLOBERTORPES"
}
export const DATE_OPTIONS = {
    hour12: false,
    year: "numeric",
    month: "2-digit",
    day: "2-digit",
    hour: "2-digit",
    minute: "2-digit"
}

export const JUGADORS = [
    {
        name: 'Esteban',
        number: 0,
        tecniques: {
            anti: 0,
            tec: 0,
            exps: 1
        }
    },
    {
        name: 'Angulo',
        number: 1,
        tecniques: {
            anti: 0,
            tec: 0,
            exps: 0
        }
    },
    {
        name: 'Manelovich',
        number: 5,
        tecniques: {
            anti: 1,
            tec: 0,
            exps: 0
        }
    },
    {
        name: 'Minimuu',
        number: 8,
        tecniques: {
            anti: 0,
            tec: 0,
            exps: 0
        }
    },
    {
        name: 'Carlos RP',
        number: 9,
        tecniques: {
            anti: 2,
            tec: 1,
            exps: 1
        }
    },
    {
        name: 'Milà',
        number: 10,
        tecniques: {
            anti: 0,
            tec: 0,
            exps: 0
        }
    },
    {
        name: 'Matteo',
        number: 11,
        tecniques: {
            anti: 0,
            tec: 2,
            exps: 1
        }
    },
    {
        name: 'Marc',
        number: 12,
        tecniques: {
            anti: 0,
            tec: 0,
            exps: 0
        }
    },
    {
        name: 'Aleix',
        number: 13,
        tecniques: {
            anti: 0,
            tec: 0,
            exps: 0
        }
    },
    {
        name: 'Domènech',
        number: 14,
        tecniques: {
            anti: 0,
            tec: 0,
            exps: 0
        }
    },
    {
        name: 'J.Elena',
        number: 23,
        tecniques: {
            anti: 1,
            tec: 0,
            exps: 0
        }
    },
    {
        name: 'Falcó',
        number: 31,
        tecniques: {
            anti: 0,
            tec: 0,
            exps: 0
        }
    },
    {
        name: 'M.Vilamajor',
        number: 32,
        tecniques: {
            anti: 0,
            tec: 0,
            exps: 0
        }
    },
    {
        name: 'Rubio',
        number: 41,
        tecniques: {
            anti: 0,
            tec: 0,
            exps: 0
        }
    },
    {
        name: 'Vivija',
        number: 77,
        tecniques: {
            anti: 0,
            tec: 0,
            exps: 0
        }
    },
];

export const FOTOS = [
    { href:"/static/images/team/gt1.jpg", width:2040, height:1536},
    { href:"/static/images/team/gt2.jpg", width:1204, height:1600},
    { href:"/static/images/team/gt3.jpg", width:2000, height:1125},
]

export const PARTITS_GLOB = [
    { partitLocal: false, puntsGlob: 29, puntsAltre: 24,equipAltre: EQUIPS.GhostBill, data: new Date(2023, 9, 29, 10, 0)},
    { partitLocal: true, puntsGlob: 31, puntsAltre: 37,equipAltre: EQUIPS.LastGame, data: new Date(2023, 10, 2, 21, 30)},
    { partitLocal: false, puntsGlob: 30, puntsAltre: 51,equipAltre: EQUIPS.Toro, data: new Date(2023, 10, 9, 21, 30)},
    { partitLocal: true, puntsGlob: 44, puntsAltre: 27,equipAltre: EQUIPS.LionsSP, data: new Date(2023, 10, 16, 20, 30)},
    { partitLocal: false, puntsGlob: 61, puntsAltre: 31,equipAltre: EQUIPS.Joan23, data: new Date(2023, 10, 23, 21, 30)},
    { partitLocal: true, puntsGlob: 55, puntsAltre: 49,equipAltre: EQUIPS.Canaris, data: new Date(2023, 10, 30, 21, 30)},
    { partitLocal: false, puntsGlob: 41, puntsAltre: 30,equipAltre: EQUIPS.Ayr, data: new Date(2023, 11, 14, 20, 30)},
    { partitLocal: true, puntsGlob: 49, puntsAltre: 40,equipAltre: EQUIPS.Riferez, data: new Date(2024, 0, 18, 20, 30)},
    { partitLocal: false, puntsGlob: 45, puntsAltre: 40,equipAltre: EQUIPS.Basf, data: new Date(2024, 0, 23, 22)},
    { partitLocal: true, puntsGlob: 53, puntsAltre: 32,equipAltre: EQUIPS.Outsiders, data: new Date(2024, 0, 30, 21)},
    { partitLocal: false, puntsGlob: 27, puntsAltre: 42,equipAltre: EQUIPS.Covestro, data: new Date(2024, 1, 11, 9)},
    { partitLocal: true, puntsGlob: 38, puntsAltre: 35,equipAltre: EQUIPS.GhostBill, data: new Date(2024, 1, 25, 10)},
    { partitLocal: false, puntsGlob: 42, puntsAltre: 49,equipAltre: EQUIPS.LastGame, data: new Date(2024, 1, 29, 21, 30)},
    { partitLocal: true, puntsGlob: 42, puntsAltre: 52,equipAltre: EQUIPS.Toro, data: new Date(2024, 2, 7, 21, 30)},
    { partitLocal: false, puntsGlob: 54, puntsAltre: 30,equipAltre: EQUIPS.LionsSP, data: new Date(2024, 2, 14, 20, 30)},
    { partitLocal: true, puntsGlob: 44, puntsAltre: 29,equipAltre: EQUIPS.Joan23, data: new Date(2024, 2, 21, 21, 30)},
    { partitLocal: false, puntsGlob: 43, puntsAltre: 51,equipAltre: EQUIPS.Canaris, data: new Date(2024, 3, 11, 21, 30)},
    { partitLocal: true, puntsGlob: 21, puntsAltre: 33,equipAltre: EQUIPS.Ayr, data: new Date(2024, 3, 18, 20, 30)},
    { partitLocal: false, puntsGlob: 45, puntsAltre: 39,equipAltre: EQUIPS.Riferez, data: new Date(2024, 3, 23, 21)},
    { partitLocal: true, puntsGlob: 48, puntsAltre: 40,equipAltre: EQUIPS.Basf, data: new Date(2024, 3, 30, 22)},
    { partitLocal: false, puntsGlob: 47, puntsAltre: 36,equipAltre: EQUIPS.Outsiders, data: new Date(2024, 4, 7, 21)},
    { partitLocal: true, puntsGlob: 41, puntsAltre: 51,equipAltre: EQUIPS.Covestro, data: new Date(2024, 4, 14, 21)},

]
export const PARTITS_FULL = [
    {
        dates: {min: new Date(2023, 10, 24), max: new Date(2023, 10, 29)},
        resultat: "24 - 29",
        guanyat: true,
        partits: [
            { equipLocal: EQUIPS.Canaris, equipVis: EQUIPS.Ayr, data: new Date(2023, 10, 24, 21, 0)},
            { equipLocal: EQUIPS.Joan23, equipVis: EQUIPS.Riferez, data: new Date(2023, 10, 24, 22, 0)},
            { equipLocal: EQUIPS.LionsSP, equipVis: EQUIPS.Basf, data: new Date(2023, 10, 26, 20, 30)},
            { equipLocal: EQUIPS.Toro, equipVis: EQUIPS.Outsiders, data: new Date(2023, 10, 26, 21, 30)},
            { equipLocal: EQUIPS.LastGame, equipVis: EQUIPS.Covestro, data: new Date(2023, 10, 29, 9, 0, 9, 0)},
            { equipLocal: EQUIPS.GhostBill, equipVis: EQUIPS.Globertorpes, data: new Date(2023, 10, 29, 10, 0)},
        ]
    },
    {
        dates: {min: new Date(2023, 10, 31), max: new Date(22023, 11, 5)},
        resultat: "31 - 37",
        guanyat: false,
        glober_data: new Date(2023, 11, 2, 21, 30),
        partits: [
            { equipLocal: EQUIPS.Ayr, equipVis: EQUIPS.GhostBill, data: new Date(2023, 10, 31, 21, 0)},
            { equipLocal: EQUIPS.Riferez, equipVis: EQUIPS.Canaris, data: new Date(2023, 10, 31, 22, 0)},
            { equipLocal: EQUIPS.Basf, equipVis: EQUIPS.Joan23, data: new Date(2023, 11, 2, 20, 30)},
            { equipLocal: EQUIPS.Globertorpes, equipVis: EQUIPS.LastGame, data: new Date(2023, 11, 2, 21, 30)},
            { equipLocal: EQUIPS.Covestro, equipVis: EQUIPS.Toro, data: new Date(2023, 11, 5, 9, 0)},
            { equipLocal: EQUIPS.Outsiders, equipVis: EQUIPS.LionsSP, data: new Date(2023, 11, 5, 10, 0)},
        ]
    }
]
