---
layout: ../layouts/RootLayout.astro
title: 'PlayIn/PlayOffs'
---

Buenos días, a continuación os enviamos el calendario para el Play IN y Play OFF de esta temporada, donde participaran todos los equipos:

## PLAY IN

__Lunes 20 de mayo__

20:30h 7 vs 8  (el que gana entra como séptimo al playoff) el que pierda tiene una nueva oportunidad
21:30h 11 vs 12

__Martes 21 de mayo__

21h: 9 vs 10 (el que gana juega el miércoles)

__Miércoles 22 de mayo__

20:30h: Perdedor del partido 7 vs 8 vs Ganador del partido 9 vs 10 ( el que gane este partido entra como Octavo en el Playoff)


## PLAY OFF

### __Jueves 23 de mayo__

20:30h: Cuartos de final ( 3 vs 6 )
21:30h: Cuartos de final ( 4 vs 5 )

### __Viernes 24 de mayo__

20:30 Cuartos de final ( 1 vs 8 )
21:30 Cuartos de final ( 2 vs 7)

__Martes 28 de mayo__

21:00 Primera semifinal
22:00 Segunda semifinal

__Jueves 30 de Mayo__

20:30 FINAL
21:30 3-4 LUGAR