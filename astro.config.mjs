import {defineConfig} from 'astro/config';
import mdx from '@astrojs/mdx';
import sitemap from '@astrojs/sitemap';

import tailwind from "@astrojs/tailwind";

import {SITE_METADATA} from "./src/consts.ts";

// https://astro.build/config
export default defineConfig({
    site: SITE_METADATA.siteUrl,
    outDir: "public",
    publicDir: "static",
    integrations: [mdx(), sitemap(), tailwind()]
});
